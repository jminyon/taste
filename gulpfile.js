var gulp = require('gulp'),
    connect = require('gulp-connect'),
    gutil = require('gulp-util'),
    protractor = require('gulp-protractor').protractor,
    webdriver_update = require("gulp-protractor").webdriver_update,
    webdriver_standalone = require("gulp-protractor").webdriver_standalone,
    server = require('karma').Server,
    watchify = require('watchify'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    path = require('path'),
    del = require('del'),
    _ = require('lodash'),
    globby = require('globby'),
    run_sequence = require('run-sequence');

var uiAssets = ['app/**/*.html', 'app/styles/**', '!app/**/README.md', '!app/bower_components'];
var bowerAssets = ['app/bower_components/**'];

gulp.task('watch', ['local', 'watchify-karma-tests', 'watchify-e2e-tests', 'watchify-site', 'ui-assets', 'bower-assets']);

gulp.task('webupdate', webdriver_update);

gulp.task('webstart',  webdriver_standalone);

gulp.task('local', function() {
    connect.server({
        root: 'dist/app/',
        port: 8888
    });
});

gulp.task('test', function(done) {
    new server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done).start();
});

gulp.task('test-cont', function(done) {
    new server({
        configFile: __dirname + '/karma.conf.js'
    }, done).start();
});

gulp.task('e2e', function() { 
    gulp.src(["dist/app/bundle_e2e.js"])
       .pipe(protractor({
           configFile: "conf.js",
           args: ['--baseUrl', 'http://127.0.0.1:8888']
       }))
       .on('error', function(e) { throw e; });
});

gulp.task('browserify', function() {
    return browserify('./app/app.js')
            .transform('browserify-ngannotate')
            .bundle()
            .pipe(source('bundle.js'))
            .pipe(gulp.dest('./dist/app'));
});

gulp.task('watchify-site', function() {
    return watchifyCreator('./app/app.js', 'bundle.js');
});

gulp.task('watchify-karma-tests', function() {
    var bundleName = 'bundle_karma.js';
    var testFiles = globby.sync(['./app/**/*.test.js', '!./dist/app/' + bundleName]);
    return watchifyCreator(testFiles, bundleName);
});

gulp.task('watchify-e2e-tests', function() {
    var bundleName = 'bundle_e2e.js';
    var testFiles = globby.sync(['./app/tests/*.js', '!./dist/app/' + bundleName]);
    return watchifyCreator(testFiles, bundleName);
});

gulp.task('ui-assets', function() {
    return gulp.src(uiAssets, {base: '.'})
                .pipe(gulp.dest('dist'));
});

gulp.task('bower-assets', function() {
    return gulp.src(bowerAssets, {base: '.'})
                .pipe(gulp.dest('dist'));
});

gulp.task('clean-dist', function(cb) {
    del('./dist/*', function() {
        cb();
    });
});

function watchifyCreator(files, bundleName) {
    var watchified = watchify(browserify(files, _.extend({debug: true}, watchify.args)));
    watchified
            .transform('browserify-ngannotate')
            .transform('require-globify');

    function update() {
        return watchified
                .bundle()
                .on('error', gutil.log.bind(gutil, 'Browserify error'))
                .pipe(source(bundleName))
                .pipe(gulp.dest('./dist/app'));
    }

    watchified.on('update', function() {
        update();
        run_sequence('test', 'e2e');
    });
    watchified.on('log', gutil.log);

    return update();
}
