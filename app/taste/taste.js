var tasteController = require("./taste.ctrl");
var tasteService = require("./taste.serv");

var taste = angular.module('taste', []);

taste.factory('tasteService', [tasteService]);

taste.controller('tasteController', ['$scope', 'tasteService', tasteController]);